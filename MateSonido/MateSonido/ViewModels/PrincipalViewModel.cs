﻿namespace MateSonido.ViewModels
{
    using System.Windows.Input;
    using Views;
    using Xamarin.Forms;
    class PrincipalViewModel : BaseViewModel
    {
        //#region Eventos
        //public event PropertyChangedEventHandler PropertyChanged;
        //#endregion
        // al incorporar la interfaz a BaseViewMOdel ya no necesito los eventos

        #region Atributos
        private bool isEnabled;
        #endregion

        #region Propiedades
        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { SetValue(ref this.isEnabled, value); }

            //get
            //{
            //    return this.isEnabled;
            //}
            //set
            //{
            //    if (this.isEnabled != value)
            //    {
            //        this.isEnabled = value;
            //        PropertyChanged?.Invoke(
            //            this,
            //            new PropertyChangedEventArgs(nameof(this.IsEnabled)));
            //    }

            //}
            // esto tambien modifico ya que meti todo en una sola clase para opotimizar codigo
        }
        #endregion
        #region Constructor
        public PrincipalViewModel()
        {
            this.IsEnabled = true;
        }
        #endregion
        #region Comandos
        public ICommand ConfigCommand
        {
            get
            {
                return new Command(Config);
            }
        }
        private async void Config()
        {
            this.isEnabled = false;

            MainViewModel.GetInstance().Menu = new MenuViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new MenuPage());

            //await Application.Current.MainPage.DisplayAlert("Titulo: vas bien",
            //    "Mensaje: Entraras a configurar la aplicacion",
            //    "Boton: Aceptar");
            //this.isEnabled = true;
            // return;
        }


        public ICommand SeguirCommand
        {
            get;
            set;
        }
        public ICommand SalirCommand
        {
            get;
            set;
        }
        #endregion
    }
}
