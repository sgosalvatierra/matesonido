﻿namespace MateSonido.ViewModels
{
using System;
using System.Windows.Input;
using Xamarin.Forms;
using Views;
   public class LingIndividualViewModel
    {
        //public ICommand VolverCommand
        //{
        //    get
        //    {
        //        return new Command(Volver);
        //    }
        //}

        //private async void Volver(object obj)
        //{

        //}

        public ICommand SelectaCommand
        {
            get
            {
                return new Command(Selecta);
            }
        }

        private async void Selecta()
        {
            MainViewModel.GetInstance().LingAPage = new LingAViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new LingAPage());
        }
    }
}
