﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MateSonido.ViewModels
{
    using System.Windows.Input;
    using Xamarin.Forms;
    using Views;
    class MenuViewModel
    {
        public ICommand SelectLingCommand
        {
            get
            {
                return new Command(SelectLing);
            }
        }

        private async void SelectLing()
        {
            MainViewModel.GetInstance().LingIndividual = new LingIndividualViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new LingIndividual());
        }
    }
}
 