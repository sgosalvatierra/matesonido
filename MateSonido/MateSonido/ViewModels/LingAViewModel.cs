﻿namespace MateSonido.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Text;
    using System.Windows.Input;
    using Xamarin.Forms;
    public class LingAViewModel : BaseViewModel
    {
        #region Atributos
        private int minuto;
        private int segundo;

        public event Action Completed;
        public event Action Ticked;
        // private DateTime time;

        #endregion
        public int Minuto
        {
            get { return this.minuto; }
            set { SetValue(ref this.minuto, value); }
        }
        public int Segundo
        {
            get { return this.segundo; }
            set { SetValue(ref this.segundo, value); }
        }

        public LingAViewModel()
        {
            var Time = TimeSpan.Zero;
            var TiempoTotal = new TimeSpan(0, 1, 30);
            var TiempoRestante = new TimeSpan();
            TiempoRestante = TimeSpan.Zero;

            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                Time += TimeSpan.FromSeconds(1);
                TiempoRestante = (TiempoTotal - Time);
                Minuto = TiempoRestante.Minutes;
                Segundo = TiempoRestante.Seconds;
                var ticked = Time.TotalSeconds < TiempoTotal.TotalSeconds;
                if (ticked)
                {
                    Ticked?.Invoke();
                }
                else
                {
                    Completed?.Invoke();
                }
                return ticked;
            });
        }
        public ICommand DetenerCommand
        {
            get
            {
                return new Command(Detener);
            }
        }

        private void Detener()
        {

        }
    }
}
