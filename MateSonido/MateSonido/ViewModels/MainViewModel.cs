﻿namespace MateSonido.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    class MainViewModel
    {
        #region ViewsModels
        public PrincipalViewModel Principal
        {
            get;
            set;
        }
        public MenuViewModel Menu
        {
            get;
            set;
        }
        public LingIndividualViewModel LingIndividual
        {
            get;
            set;
        }
        public LingAViewModel LingAPage
        {
            get;
            set;
        }
        #endregion
        #region Constructores
        public MainViewModel()
        {
            instance = this;
            this.Principal = new PrincipalViewModel();
           
        }
        #endregion
        #region Singleton
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion
    }
}
